const { response } = require('express');

const usersGet = (req, res = response) => {
    res.send('Hello World controlador');
}

const usersPost = (req, res = response) => {

    const body = req.body;

    res.json({
        body
    });
}

module.exports = {
    usersGet,
    usersPost
}